from django.db import models
from django.contrib.auth.models import User
from django.db.models import signals


class LocalDomain(models.Model):
    local_domain_id = models.AutoField(null=False, primary_key=True)
    domain_name = models.CharField(max_length=100, null=False, blank=False)
    path = models.CharField(max_length=512, null=False, blank=False)
    unix_user = models.IntegerField(null=False, blank=False, default=0)
    unix_group = models.IntegerField(null=False, blank=False, default=0)

    def __str__(self):
        return self.domain_name

    trigger_update = True

    class Meta:
        ordering = ('domain_name',)
        db_table = 'mail"."local_domains'
        managed = False


class Forwarder(models.Model):
    forwarder_id = models.AutoField(null=False, primary_key=True)
    local_part = models.CharField(max_length=100, null=False, blank=False)
    local_domain = models.ForeignKey(LocalDomain, null=False, blank=False, db_column='local_domain_id', on_delete=models.CASCADE)
    remote_name = models.CharField(max_length=200, null=False, blank=False)

    def __str__(self):
        return "%s@%s -> %s" % (self.local_part, self.local_domain.domain_name, self.remote_name)

    trigger_update = True

    class Meta:
        ordering = ('local_part',)
        db_table = 'mail"."forwarder'
        managed = False


class VirtualUser(models.Model):
    virtual_user_id = models.AutoField(null=False, primary_key=True)
    local_domain = models.ForeignKey(LocalDomain, null=False, blank=False, db_column='local_domain_id', on_delete=models.CASCADE)
    local_part = models.CharField(max_length=100, null=False, blank=False)
    mail_quota = models.IntegerField(null=False)
    passwd = models.CharField(max_length=100, null=False, blank=False, verbose_name="Password")
    full_name = models.CharField(max_length=200, null=False, blank=True)

    def __str__(self):
        return "%s@%s (%s)" % (self.local_part, self.local_domain.domain_name, self.full_name or '')

    trigger_update = True

    class Meta:
        ordering = ('local_part',)
        db_table = 'mail"."virtual_user'
        managed = False
        unique_together = ('local_domain', 'local_part', )


class UserPermissions(models.Model):
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    domain = models.ForeignKey(LocalDomain, null=False, on_delete=models.CASCADE)
    pattern = models.CharField(max_length=100, null=False, blank=False)

    def __str__(self):
        return "%s -> %s pattern '%s'" % (self.user, self.domain, self.pattern)


class Log(models.Model):
    user = models.ForeignKey(User, null=False, on_delete=models.CASCADE)
    when = models.DateTimeField(null=False, auto_now=True)
    what = models.CharField(max_length=2048, null=False, blank=False)

    def __str__(self):
        return "%s (%s): %s" % (self.when, self.user, self.what)

    class Meta:
        ordering = ('-when',)


def pgmail_save_handler(sender, **kwargs):
    if hasattr(sender, 'trigger_update') and sender.trigger_update:
        # Touch a temp file in /tmp/ to inform cronjobs we have
        # changed something.
        with open('/tmp/.mailmgr_update', 'w') as f:
            pass


signals.post_save.connect(pgmail_save_handler)
