from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db import transaction


from .models import *
from .forms import *


def log(user, what):
    l = Log()
    l.user = user
    l.what = what
    l.save()


@login_required
def home(request):
    users = VirtualUser.objects.extra(where=["EXISTS (SELECT 1 FROM mailmgr_userpermissions p WHERE p.user_id=%s AND p.domain_id = local_domain_id AND local_part ~* ('^'||p.pattern||'$'))" % request.user.id])
    forwards = Forwarder.objects.extra(where=["EXISTS (SELECT 1 FROM mailmgr_userpermissions p WHERE p.user_id=%s AND p.domain_id = local_domain_id AND local_part ~* ('^'||p.pattern||'$'))" % request.user.id])

    return render(request, 'home.html', {
        'users': users,
        'forwarders': forwards,
    })


@transaction.atomic
@login_required
def userform(request, userparam):
    if userparam == 'add':
        vu = VirtualUser()
    else:
        vulist = VirtualUser.objects.filter(pk=userparam).extra(where=["EXISTS (SELECT 1 FROM mailmgr_userpermissions p WHERE p.user_id=%s AND p.domain_id = local_domain_id AND local_part ~* ('^'||p.pattern||'$'))" % request.user.id])
        if len(vulist) != 1:
            raise Http404("Not found or no permissions!")
        vu = vulist[0]

    if request.method == 'POST':
        form = VirtualUserForm(data=request.POST, instance=vu, user=request.user)
        if request.POST['passwd'] != vu.passwd:
            password_changed = True
        else:
            password_changed = False
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.INFO, 'User %s updated' % vu)
            if password_changed:
                messages.add_message(request, messages.INFO, 'Password changed for user %s' % vu)
                log(request.user, "%s user %s, including changing the password" % (userparam == 'add' and 'Added' or 'Updated', vu))
            else:
                log(request.user, "%s user %s, without changing the password" % (userparam == 'add' and 'Added' or 'Updated', vu))
            return HttpResponseRedirect('/')
    else:
        # Generate a new form
        form = VirtualUserForm(instance=vu, user=request.user)

    return render(request, 'form.html', {
        'form': form,
        'savebutton': (userparam == 'new') and "New" or "Save",
        'cancelurl': '/',
    })


@transaction.atomic
@login_required
def forwarderform(request, userparam):
    if userparam == 'add':
        fwd = Forwarder()
    else:
        fwdlist = Forwarder.objects.filter(pk=userparam).extra(where=["EXISTS (SELECT 1 FROM mailmgr_userpermissions p WHERE p.user_id=%s AND p.domain_id = local_domain_id AND local_part ~* ('^'||p.pattern||'$'))" % request.user.id])
        if len(fwdlist) != 1:
            raise Http404("Not found or no permissions!")
        fwd = fwdlist[0]

    if request.method == 'POST':
        form = ForwarderForm(data=request.POST, instance=fwd, user=request.user)
        if form.is_valid():
            form.save()
            log(request.user, "%s forwarding %s -> %s" % (userparam == 'add' and 'Added' or 'Updated', fwd, fwd.remote_name))
            messages.add_message(request, messages.INFO, 'Forwarder %s updated' % fwd)
            return HttpResponseRedirect('/')
    else:
        # Generate a new form
        form = ForwarderForm(instance=fwd, user=request.user)

    return render(request, 'form.html', {
        'form': form,
        'savebutton': (userparam == 'new') and "New" or "Save",
        'cancelurl': '/#forwarders',
    })
