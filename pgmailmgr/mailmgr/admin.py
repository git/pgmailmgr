from django.contrib import admin

from .models import *

admin.site.register(LocalDomain)
admin.site.register(Forwarder)
admin.site.register(VirtualUser)
admin.site.register(UserPermissions)
admin.site.register(Log)
