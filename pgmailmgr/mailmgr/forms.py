from django import forms
from django.forms import ValidationError
from django.db import connection

from .models import *


class VirtualUserForm(forms.ModelForm):
    class Meta:
        model = VirtualUser
        fields = ('local_domain', 'local_part', 'mail_quota', 'passwd', 'full_name')

    def __init__(self, data=None, instance=None, user=None):
        super(VirtualUserForm, self).__init__(data=data, instance=instance)
        self.user = user

    def clean_local_domain(self):
        if not self.instance.pk:
            return self.cleaned_data['local_domain']
        if self.cleaned_data['local_domain'] != self.instance.local_domain:
            raise ValidationError("Can't change local domain!")
        return self.cleaned_data['local_domain']

    def clean_local_part(self):
        if not self.instance.pk:
            return self.cleaned_data['local_part']
        if self.cleaned_data['local_part'] != self.instance.local_part:
            raise ValidationError("Renaming accounts is not possible - you have to delete and add!")
        return self.cleaned_data['local_part']

    def clean_mail_quota(self):
        if self.cleaned_data['mail_quota'] <= 1:
            raise ValidationError("Mail quota must be set")
        return self.cleaned_data['mail_quota']

    def clean_passwd(self):
        if self.cleaned_data['passwd'] != self.instance.passwd:
            # Changing password requires calling pgcrypto. So let's do that...
            curs = connection.cursor()
            curs.execute("SELECT public.crypt(%(pwd)s, public.gen_salt('md5'))", {
                'pwd': self.cleaned_data['passwd']
            })
            return curs.fetchall()[0][0]

        return self.cleaned_data['passwd']

    def clean(self):
        if 'local_part' not in self.cleaned_data:
            return {}
        if 'local_domain' not in self.cleaned_data:
            return {}

        # Validate that the pattern is allowed
        curs = connection.cursor()
        curs.execute("SELECT 1 FROM mailmgr_userpermissions WHERE user_id=%(uid)s AND domain_id=%(domain)s AND %(lp)s ~* ('^'||pattern||'$')", {
            'uid': self.user.pk,
            'domain': self.cleaned_data['local_domain'].pk,
            'lp': self.cleaned_data['local_part'],
        })
        perms = curs.fetchall()

        if len(perms) < 1:
            raise ValidationError("Permission denied to create that user for that domain!")

        # If it's a new user, also check against if it already exists
        if not self.instance.pk:
            old = VirtualUser.objects.filter(local_part=self.cleaned_data['local_part'], local_domain=self.cleaned_data['local_domain'])
            if len(old):
                raise ValidationError("A user with that name already exists in that domain!")

        # Make sure we can't get a collision with a forwarding
        forwarders = Forwarder.objects.filter(local_part=self.cleaned_data['local_part'], local_domain=self.cleaned_data['local_domain'])
        if len(forwarders):
            raise ValidationError("A forwarder with that name already exists in that domain!")

        return self.cleaned_data


class ForwarderForm(forms.ModelForm):
    class Meta:
        model = Forwarder
        fields = ('local_part', 'local_domain', 'remote_name')

    def __init__(self, data=None, instance=None, user=None):
        super(ForwarderForm, self).__init__(data=data, instance=instance)
        self.user = user

    def clean_local_domain(self):
        if not self.instance.pk:
            return self.cleaned_data['local_domain']
        if self.cleaned_data['local_domain'] != self.instance.local_domain:
            raise ValidationError("Can't change local domain!")
        return self.cleaned_data['local_domain']

    def clean_local_part(self):
        if not self.instance.pk:
            return self.cleaned_data['local_part']
        if self.cleaned_data['local_part'] != self.instance.local_part:
            raise ValidationError("Renaming forwarders is not possible - you have to delete and add!")
        return self.cleaned_data['local_part']

    def clean(self):
        if 'local_part' not in self.cleaned_data:
            return {}
        if 'local_domain' not in self.cleaned_data:
            return {}

        # Validate that the pattern is allowed
        curs = connection.cursor()
        curs.execute("SELECT 1 FROM mailmgr_userpermissions WHERE user_id=%(uid)s AND domain_id=%(domain)s AND %(lp)s ~* ('^'||pattern||'$')", {
            'uid': self.user.pk,
            'domain': self.cleaned_data['local_domain'].pk,
            'lp': self.cleaned_data['local_part'],
        })
        perms = curs.fetchall()

        if len(perms) < 1:
            raise ValidationError("Permission denied to create that forwarder for that domain!")

        # If it's a new user, also check against if it already exists
        if not self.instance.pk:
            old = Forwarder.objects.filter(local_part=self.cleaned_data['local_part'], local_domain=self.cleaned_data['local_domain'])
            if len(old):
                raise ValidationError("A forwarder with that name already exists in that domain!")

        # Make sure we can't get a collision with a user
        users = VirtualUser.objects.filter(local_part=self.cleaned_data['local_part'], local_domain=self.cleaned_data['local_domain'])
        if len(users):
            raise ValidationError("A user with that name already exists in that domain!")

        return self.cleaned_data
