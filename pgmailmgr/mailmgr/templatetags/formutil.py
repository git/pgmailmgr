from django import template

register = template.Library()


@register.filter(is_safe=True)
def label_class(value, arg):
    return value.label_tag(attrs={'class': arg})


@register.filter(is_safe=True)
def field_class(value, arg):
    prevclass = value.field.widget.attrs.get('class', '')
    if prevclass:
        newclass = "{0} {1}".format(arg, prevclass)
    else:
        newclass = arg
    return value.as_widget(attrs={"class": newclass})
