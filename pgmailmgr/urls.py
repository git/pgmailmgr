from django.urls import re_path

import pgmailmgr.mailmgr.views as views
import pgmailmgr.auth

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    re_path(r'^$', views.home),
    re_path(r'^user/(\d+|add)/$', views.userform),
    re_path(r'^forwarder/(\d+|add)/$', views.forwarderform),

    # Auth
    re_path('^auth_receive/$', pgmailmgr.auth.auth_receive),
    re_path('^auth_api/$', pgmailmgr.auth.auth_api),
    re_path('^accounts/logout/$', pgmailmgr.auth.logout),
    re_path('^accounts/login/$', pgmailmgr.auth.login),

    # Uncomment the next line to enable the admin:
    re_path(r'^admin/', admin.site.urls),
]
